#!/usr/bin/rdmd
import std.stdio;
import std.conv;
import std.regex;
import std.string;
import std.algorithm;
import core.stdc.stdlib;

dstring[] load(File f) {
    dstring[] lines = [];
    foreach (line; f.byLine) {
        lines ~= to!dstring(line.strip);
    }
    return lines;
}

dstring[] removeComment(dstring[] lines) {
    dstring[] rslt;
    foreach (line; lines) {
        rslt ~= line.findSplitBefore("%")[0];
    }
    return rslt;
}

dstring[] pickDocument(dstring[] lines) {
    dstring[] rslt;
    if (auto r = lines.findSplitAfter(["\\begin{document}"d])) {
        rslt = r[1];
    } else {
        stderr.writeln("No document");
        exit(1);
    }
    if (auto r = rslt.findSplitBefore(["\\end{document}"d])) {
        rslt = r[0];
    } else {
        stderr.writeln("No document");
        exit(1);
    }
    return rslt;
}

dstring[] convertSection(dstring[] lines, bool add_number = true) {
    dstring[] rslt = [];
    int number = 1;
    auto r = regex(r"\\section\{(.*)\}"d);
    foreach (line; lines) {
        if (auto m = match(line, r)) {
            auto str = add_number ? to!dstring(number++) ~ "  "d : ""d;
            str ~= m.captures[1];
            rslt ~= ""d;
            rslt ~= "[chapter:"d ~ str ~ "]"d;
            rslt ~= ""d;
        } else {
            rslt ~= line;
        }
    }
    return rslt;
}

dstring[] convertKeyword(dstring[] lines) {
    struct Pattern {
        Regex!dchar r;
        dstring to;
        this(Regex!dchar r, dstring to) {
            this.r = r;
            this.to = to;
        }
    }

    auto pattern = [Pattern(regex(r"---"d, "g"), "ー"d), Pattern(regex(r"~~"d, "g"), "　")];
    foreach (ref line; lines) {
        foreach (p; pattern) {

            line = replace(line, p.r, p.to);
        }
    }
    return lines;
}

dstring[] removeTexWord(dstring[] lines) {
    dstring[] rslt = [];
    auto r = regex(r"\\.+"d);
    foreach (line; lines) {
        if (match(line, r)) {
            continue;
        }
        rslt ~= line;
    }
    return rslt;
}

dstring[] addIndent(dstring[] lines) {
    dstring[] rslt = [];
    bool brank = true;
    auto keyword = regex(r"\[chapter.+\]"d);
    auto bracket = regex(r"^「"d);
    foreach (line; lines) {
        dstring str = ""d;
        if (line == ""d) {
            brank = true;
        } else if (match(line, bracket)) {
            brank = false;
        } else if (brank && !match(line, keyword)) {
            str ~= "　"d;
            brank = false;
        }
        rslt ~= str ~ line;
    }
    return rslt;
}

dstring[] concat(dstring[] lines) {
    dstring[] rslt = [];
    dstring str = ""d;
    bool br = true;
    foreach (line; lines) {
        if (line == ""d) {
            if (!br) {
                rslt ~= str;
                str = ""d;
                br = true;
            }

            continue;
        } else {
            str ~= line;
            br = false;
        }
    }
    return rslt;
}

void main(string[] args) {
    foreach (line; stdin.load.removeComment.pickDocument.convertSection
            .convertKeyword.removeTexWord.addIndent.concat) {
        writeln(line);
    }

}
